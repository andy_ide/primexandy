<?php

namespace App\Http\Controllers;

use App\Models\Product;
use App\Models\Stock;
use Illuminate\Http\Request;

class ProductController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return Product::all();
    }

    public function allWithStock()
    {
        return Product::with('stock')->get();
    }

    public function indexSortAsc()
    {
        return $this->getProductsOrdered('ASC');
    }

    public function indexSortDesc()
    {
        return $this->getProductsOrdered('DESC');
    }

    protected function getProductsOrdered($sortType = 'ASC')
    {
        // We are using the database here rather than Eloquent as databases are faster at sorting.
        return Product::join('stock', 'products.id','=','stock.product_id')
            ->orderBy('stock.on_hand', $sortType)
            ->get();
    }

    public function stockAvailable()
    {
        return Product::join('stock', 'products.id','=','stock.product_id')
            ->where('stock.on_hand', '>', 0)
            ->get();
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'code' => 'required',
            'name' => 'required',
            'description' => 'required',
        ]);

        $product = Product::create([
            'code' => request('code'),
            'name' => request('name'),
            'description' => request('description'),
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Product $product)
    {
        return $product;
    }

    public function productStock(Product $product)
    {
        return $product->stock;
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Product $product)
    {
        // Use interect() so we don't have lots of if(request()->has(.....  statements.
        $product->update(
            collect(request())
                ->intersect('code', 'name', 'description')
                ->toArray()
        );
    }

    /**
     * Soft Delete the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function delete(Product $product)
    {
        $product->delete();
    }


    public function addStockOnHand(Product $product, int $on_hand)
    {
        Stock::create([
            'product_id' => $product->id,
            'on_hand' => $on_hand,
        ]);

    }
}
