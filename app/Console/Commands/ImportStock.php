<?php

namespace App\Console\Commands;

use App\Models\Product;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;

class ImportStock extends Command
{
    const RECORDS_PER_DB_WRITE = 1000;
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'import:stock {file}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Import a products CSV. eg php artisan import:products primex-stock-test.csv';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        // Keep Memory usage low
        DB::connection()->disableQueryLog();

        // We read a line at a time to keep memory usage low.
        // We then store a bunch or records in memory - before doing a commit to the DB
        // Idea is that DB writing is slow so we'd rather do bulk inserts than one by one inserts.
        $handle = fopen($this->argument('file'), 'r');
        if ($handle) {

            $numLines = 0;
            $data = [];

            while ($line = fgetcsv($handle)) {
                $numLines++;

                // We dodge the first row due to it being the column headers
                if($numLines > 1) {
                    $product_code = $line[0];
                    $on_hand = $line[1];
                    $production_date = $line[2];

                    $product = DB::table('products')->where('code', '=', $product_code)->first();

                    if(!$product) {
                        $this->info(sprintf('No product found for line %s %s', $numLines, $product_code));
                    } else {

                        $data[] = [
                            'product_id' => $product->id,
                            'on_hand' => $on_hand,
                            'production_date' => $production_date,
                            'created_at' => now(),
                            'updated_at' => now(),
                        ];

                        if ($numLines % static::RECORDS_PER_DB_WRITE == 0) {
                            DB::table('stock')->insert($data);
                            $data = [];
                            $this->info('Storing up to record: ' . $numLines);
                        }
                    }
                }
            }

            // Insert the remainder of records
            DB::table('stock')->insert($data);
            $this->info(sprintf('Storing remaining %s records', count($data)));
        }
    }
}
