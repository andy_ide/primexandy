<?php

use App\Http\Controllers\ProductController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
Route::get('products',[ProductController::class, 'index']);
Route::get('products/asc',[ProductController::class, 'indexSortAsc']);
Route::get('products/desc',[ProductController::class, 'indexSortDesc']);
Route::get('products/stock_available',[ProductController::class, 'stockAvailable']);
Route::get('product/{product}',[ProductController::class, 'show']);
Route::get('products/stock',[ProductController::class, 'allWithStock']);
Route::get('product/{product}/stock',[ProductController::class, 'productStock']);

Route::post('add',[ProductController::class, 'store']);
Route::put('update/{product}',[ProductController::class, 'update']);
Route::put('delete/{product}',[ProductController::class, 'delete']);
Route::post('add_stock_on_hand/{product}/{on_hand}',[ProductController::class, 'addStockOnHand']);
