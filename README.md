# PrimeX Code Test for Andy Ide

## Dev Notes

- I approached this by creating the migrations and models first. Then wrote tests (that obviously failed). 
  The next step was to write the code that made the tests pass. Final stage was coding the artisan commands
  to do the file imports.
  
- I made an initial error in thinking `Product` and `Stock` were a One to One relationship. I fixed this to
One to Many. (Hence why if you look at the commit history you can see this).
  
- Sqlite has been chosen as the DB so this project is all contained in one repo. Usually I would use MySql or the like.

- The requirements are fulfilled in the tests `AptTest.php`. Most other code is in `ProductController.php`

- I have been taught to remove obvious comments in the code that are often put in by the auto generators. I will keep 
these comments in as it is the Laravel 'standard'. In practice, I would follow your business policy. I do love putting
  in comments however if the name of the function is descriptive enough, comments usually aren't necessary.
  
- ProductController is getting a bit big. I would normally split this into different files, but I've
kept it in one file for convenience, so you can just whiz down it to see the code.

- The csv data files are in the root dir.

- I normally prefer to code in a Linux Ubuntu environment. 

## Setup
- Copy the `.env.example` to `.env` and update the variables for DB location and 
  generate a new app key `php artisan key:generate` - copy this key over to `.env.testing` from `.env`
    
- Double check that the `env.testing` is correct for your environment. I use Valet for dev.
- ensure the DB_DATABASE variable is the correct absolute URL for your system.


## Tests

- Run tests via: `php artisan test`

- All the tests for the requirements in Item 3 are there.

- The file to read is `ApiTest.php`


## Importing data

- The two commands to run are:

```
php artisan import:products primex-products-test.csv 
  
php artisan import:products primex-stock-test.csv
```  
  
- Due to the request for performance I've turned off the Laravel query log to reduce memory usage.
  
- We read the CSV a line at a time to keep memory usage low.  

- We then store a bunch or records in memory - before doing a commit to the DB  

- Idea is that DB writing is slow, so we'd rather do bulk inserts than one by one inserts.  

- I have favoured talking to the DB directory rather than using eloquent as calling DB is faster.

- I really enjoyed coding these commands as it is "old school".


## Conclusion

- Thank you for your time considering this test code!


