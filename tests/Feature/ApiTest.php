<?php

namespace Tests\Feature;

use App\Models\Product;
use App\Models\Stock;
use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class ApiTest extends TestCase
{
    use RefreshDatabase;

    public function test_index_route_is_available()
    {
        $response = $this->get('/');
        $response->assertStatus(200);
    }

    public function test_a_product_can_be_added()
    {
        $product = Product::factory()->make();

        $response = $this->post('api/add',
            [
                'code' => $product->code,
                'name' => $product->name,
                'description' => $product->description,
            ]
        );

        $response->assertOk();

        $this->assertDatabaseHas('products', [
            'code' => $product->code,
            'name' => $product->name,
            'description' => $product->description,
        ]);
    }

    public function test_a_product_can_be_updated()
    {
        $product = Product::factory()->make();
        $product->save();

        $response = $this->put(
            sprintf('api/update/%s/', $product->id),
            [
                'code' => "a new code",
                'name' => "a new name",
            ]
        );
        $response->assertOk();
    }

    public function test_a_product_can_be_deleted()
    {
        $product = Product::factory()->make();
        $product->save();

        $response = $this->put(
            sprintf('api/delete/%s/', $product->id)
        );

        $response->assertOk();
        $product->refresh();
        $this->assertSoftDeleted($product);
    }

    public function test_can_add_stock_on_hand_to_a_product()
    {
        $this->withoutExceptionHandling();

        $on_hand = 42;

        $product = Product::factory()->make();
        $product->save();

        $response = $this->post(
            sprintf('api/add_stock_on_hand/%s/%s', $product->id, $on_hand)
        );

        $response->assertOk();
    }

    public function test_can_get_all_products()
    {
        $code = 'a_code_im_looking_for';
        $product = Product::factory()->make(['code' => $code]);
        $product->save();

        $response = $this->get('api/products');
        $response->assertOk();
        $response->assertSee($code);
    }

    public function test_can_get_a_products_details()
    {
        $product = Product::factory()->make();
        $product->save();

        Stock::factory()->make(['product_id' => $product->id]);

        $response = $this->get(
            sprintf('api/product/%s/', $product->id)
        );
        $response->assertOk();
    }


    public function test_can_get_a_products_on_hand_details()
    {
        // I wouldn't use #on_hand and on_hand2 in production - but with tests it is best to be explicit.
        $on_hand = 314;
        $on_hand2 = 345;

        $product = Product::factory()->make();
        $product->save();

        $stock = Stock::factory()->make(['product_id' => $product->id,'on_hand' => $on_hand]);
        $stock->save();

        $stock2 = Stock::factory()->make(['product_id' => $product->id,'on_hand' => $on_hand2]);
        $stock2->save();

        $response = $this->get(
            sprintf('api/product/%s/stock', $product->id)
        );
        $response->assertOk();
        $response->assertSee($on_hand);
        $response->assertSee($on_hand2);
    }


    public function test_can_get_all_products_on_hand_details()
    {
        $on_hand = 315;
        $on_hand2 = 316;

        $product = Product::factory()->make();
        $product->save();

        $stock = Stock::factory()->make(['product_id' => $product->id,'on_hand' => $on_hand]);
        $stock->save();

        $product2 = Product::factory()->make();
        $product2->save();

        $stock2 = Stock::factory()->make(['product_id' => $product2->id,'on_hand' => $on_hand2]);
        $stock2->save();

        $response = $this->get('api/products/stock');
        $response->assertOk();
        $response->assertSee($on_hand);
        $response->assertSee($on_hand2);
    }

    public function test_can_get_products_by_stock_onhand_in_asc_order()
    {
        $on_hand = 400;
        $on_hand2 = 410;

        $product = Product::factory()->make();
        $product->save();

        $stock = Stock::factory()->make(['product_id' => $product->id,'on_hand' => $on_hand]);
        $stock->save();

        $product2 = Product::factory()->make();
        $product2->save();

        $stock2 = Stock::factory()->make(['product_id' => $product2->id,'on_hand' => $on_hand2]);
        $stock2->save();

        $response = $this->get('api/products/asc');
        $response->assertOk();
        $response->assertSeeTextInOrder([$on_hand, $on_hand2]);
    }

    public function test_can_get_products_by_stock_onhand_in_desc_order()
    {
        $on_hand = 500;
        $on_hand2 = 450;

        $product = Product::factory()->make();
        $product->save();

        $stock = Stock::factory()->make(['product_id' => $product->id,'on_hand' => $on_hand]);
        $stock->save();

        $product2 = Product::factory()->make();
        $product2->save();

        $stock2 = Stock::factory()->make(['product_id' => $product2->id,'on_hand' => $on_hand2]);
        $stock2->save();

        $response = $this->get('api/products/desc');
        $response->assertOk();
        $response->assertSeeTextInOrder([$on_hand, $on_hand2]);
    }

    public function test_can_get_products_with_stock_onhand()
    {
        $on_hand = 1000;
        $on_hand2 = 0;

        $product = Product::factory()->make();
        $product->save();

        $stock = Stock::factory()->make(['product_id' => $product->id,'on_hand' => $on_hand]);
        $stock->save();

        $product2 = Product::factory()->make();
        $product2->save();

        $stock2 = Stock::factory()->make(['product_id' => $product2->id,'on_hand' => $on_hand2]);
        $stock2->save();

        $response = $this->get('api/products/stock_available');
        $response->assertOk();
        $response->assertSee($product->code);
        $response->assertDontSee($product2->code);
    }
}
